production:
	sudo -E docker stack deploy --prune -c docker-compose.yml example

staging: test
	sudo -E docker stack deploy --prune -c docker-compose.yml -c docker-compose.staging.yml example_$$CI_COMMIT_REF_SLUG

cleanup:
	sudo docker stack rm example_$$CI_COMMIT_REF_SLUG

test:
	echo example_$$CI_COMMIT_REF_SLUG
